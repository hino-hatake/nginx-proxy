## I. Some handy commands

##### Generate self-signed certs
```bash
./gen-self-signed-cert.sh whoami.local
./gen-self-signed-cert.sh whoareyou.local
```

##### Generate `letsencrypt` certs
Make sure port 80 is available, then:
```bash
sudo certbot certonly --standalone --agree-tos -n -m hino@hino.one -d who.hino.one
sudo certbot certonly --standalone --agree-tos -n -m hino@hino.one -d hideout.hino.one
```

##### Generate basic auth htpasswd file
```bash
htpasswd -db htpasswd/hideout.local hino secret
```

##### Verify htpasswd file
```bash
htpasswd -vb htpasswd/hideout.local hino secret
```

##### Keep `nginx.tmpl` up-to-date
```bash
wget https://github.com/nginx-proxy/nginx-proxy/raw/main/nginx.tmpl -qO nginx/nginx.tmpl
curl -sL https://github.com/nginx-proxy/nginx-proxy/raw/main/nginx.tmpl > nginx/nginx.tmpl
```

## II. Something good to know

A good site to test your SSL setup: https://www.ssllabs.com/ssltest/

Take a look at my site `who.hino.one` with Overall Rating A: https://www.ssllabs.com/ssltest/analyze.html?d=who.hino.one

##### Enable OCSP Stapling
Take `who.hino.one` as an example, the config would look like this:
```nginx
ssl_session_timeout 5m;
ssl_session_cache shared:SSL:50m;
ssl_session_tickets off;

ssl_certificate /etc/nginx/certs/who.hino.one.crt;
ssl_certificate_key /etc/nginx/certs/who.hino.one.key;

# OCSP stapling
ssl_stapling on;
ssl_stapling_verify on;

# verify chain of trust of OCSP response using Root CA and Intermediate certs
ssl_trusted_certificate /etc/nginx/certs/who.hino.one.chain.pem;
```
If you generate the cert with `letsencrypt`, use `chain1.pem` and name it `your-domain.chain.pem` as `ssl_trusted_certificate`.

Verify it online here: https://www.digicert.com/help/

Another way to verify:
```bash
openssl s_client -connect localhost:443 -servername who.hino.one -status < /dev/null | grep 'OCSP Response Status'
```
If OCSP stapling is enabled, the response whould say:
```
OCSP Response Status: successful (0x0)
```

##### SSL Policy
Read more here: https://wiki.mozilla.org/Security/Server_Side_TLS

How the config would look like if we use Intermediate compatibility as recommended:
```nginx
# intermediate configuration
ssl_protocols TLSv1.2 TLSv1.3;
ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
ssl_prefer_server_ciphers off;
```

##### HSTS
Simply put, allowing web servers to declare that web browsers (or other complying user agents) should automatically interact with it using only HTTPS connections.
```nginx
# HSTS (ngx_http_headers_module is required) (63072000 seconds)
add_header Strict-Transport-Security "max-age=63072000" always;
```
Clear HSTP for Chrome: chrome://net-internals/#hsts

## III. ACME Companion Utilities

The ACME Companion container provide the following utilities (replace `nginx-proxy-acme` with the name or ID of your **acme-companion** container when executing the commands):

##### Force certificates renewal
If needed, you can force a running **acme-companion** container to renew all certificates that are currently in use with the following command:

```bash
docker exec nginx-proxy-acme /app/force_renew
```

##### Manually trigger the service loop
You can trigger the execution of the service loop before the hourly execution with:

```bash
docker exec nginx-proxy-acme /app/signal_le_service
```
Unlike the previous command, this won't force renewal of certificates that don't need to be renewed.

##### Show certificates informations
To display informations about your existing certificates, use the following command:

```bash
docker exec nginx-proxy-acme /app/cert_status
```

##### Trick to update renew time manually
Access `/etc/acme.sh/<your-email>/<your-domain>/<your-domain>.conf` inside `nginx-proxy-acme` container, you would see something similar to:
```properties
Le_CertCreateTime='1624397626'
Le_CertCreateTimeStr='Tue Jun 22 21:33:46 UTC 2021'
Le_NextRenewTimeStr='Sat Aug 21 21:33:46 UTC 2021'
Le_NextRenewTime='1629495226'
```
Update `Le_NextRenewTime` with your desired unix timestamp (`Le_NextRenewTimeStr` is optional), and trigger the service loop like above.

##### Change number of days to renew
By default it's `60`, which is set inside the `acme.sh` script:
```properties
DEFAULT_RENEW=60
```
In case you want to change it, access `/etc/acme.sh/<your-email>/<your-domain>/<your-domain>.conf` inside `nginx-proxy-acme` container, and add the line below:
```properties
Le_RenewalDays='69'
```
Because this config file is generated after the first run (creating certs), so it will only take effect from the second run onward (`Le_NextRenewTime` had been calculated already). Therefore, you may still need to update `Le_NextRenewTime` and trigger the service loop once.
